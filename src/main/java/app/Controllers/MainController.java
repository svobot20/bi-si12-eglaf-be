package app.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    /**
     * Index page
     *
     * @return api running string
     */
    @RequestMapping("/")
    public String index() {
        return "API server running.";
    }

}
