package app.Controllers;

import app.DTO.LoginDTO;
import app.DTO.PartimeWorkerDTO;
import app.Services.LoginServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    private LoginServiceInterface loginService;

    @Autowired
    public LoginController(LoginServiceInterface loginService) {
        this.loginService = loginService;
    }

    /**
     * Login worker
     *
     * @param loginDTO login worker data
     * @return worker data
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public PartimeWorkerDTO login(@RequestBody LoginDTO loginDTO) {
        return loginService.login(loginDTO);
    }

}
