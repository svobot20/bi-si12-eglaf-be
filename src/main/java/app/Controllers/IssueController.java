package app.Controllers;

import app.DTO.*;
import app.Services.IssueServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class IssueController {
    private IssueServiceInterface issueService;

    @Autowired
    public IssueController(IssueServiceInterface issueService) {
        this.issueService = issueService;
    }

    /**
     * Get all open unassigned issues
     *
     * @return list of open issues
     */
    @GetMapping("/issues")
    public List<IssueDTO> getIssues() {
        return issueService.getOpenIssues();
    }

    /**
     * Get list of assigned issues
     *
     * @param getIssuesDTO contains user id
     * @return list of assigned issues
     */
    @PostMapping("/issues/claimed")
    public List<IssueDTO> getClaimedIssues(@RequestBody GetIssuesDTO getIssuesDTO) {
        return issueService.getClaimedIssues(getIssuesDTO.getUserId());
    }

    /**
     * Assign issue to user
     *
     * @param issueManipulationDTO issue data to be assigned to user
     * @return http status OK if operation succeeded
     */
    @PostMapping("/claim-issue")
    public ResponseEntity claimIssue(@RequestBody IssueManipulationDTO issueManipulationDTO) {
        boolean ok = issueService.claimIssue(issueManipulationDTO);

        if (ok) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Un-assign issue from user
     *
     * @param issueManipulationDTO issue data to be un-assigned from user
     * @return http status OK if operation succeeded
     */
    @PostMapping("/unclaim-issue")
    public ResponseEntity unclaimIssue(@RequestBody IssueManipulationDTO issueManipulationDTO) {
        boolean ok = issueService.unclaimIssue(issueManipulationDTO);

        if (ok) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Report new issue
     *
     * @param reportIssueDTO issue data
     * @return http status OK if operation succeeded
     */
    @PostMapping("/report-issue")
    public ResponseEntity reportIssue(@RequestBody ReportIssueDTO reportIssueDTO) {
        // TODO: use session to get user
        issueService.reportIssue(reportIssueDTO);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Cancel issue
     *
     * @param cancelIssueDTO issue to be canceled
     * @return http status OK if operation succeeded
     */
    @PostMapping("/cancel-issue")
    public ResponseEntity cancelIssue(@RequestBody CancelIssueDTO cancelIssueDTO) {
        boolean ok = issueService.cancelIssue(cancelIssueDTO);

        if (ok) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Resolve issue
     *
     * @param issueManipulationDTO issue to be resolved
     * @return http status OK if operation succeeded
     */
    @PostMapping("/resolve-issue")
    public ResponseEntity resolveIssue(@RequestBody IssueManipulationDTO issueManipulationDTO) {
        boolean ok = issueService.resolveIssue(issueManipulationDTO);

        if (ok) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
