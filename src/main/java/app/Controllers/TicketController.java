package app.Controllers;

import app.DTO.TicketControlDTO;
import app.Services.TicketServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TicketController {
    private TicketServiceInterface ticketService;

    @Autowired
    public TicketController(TicketServiceInterface ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * Check if ticket is valid
     *
     * @param ticketControlDTO ticket data
     * @return http OK if ticket is valid
     */
    @PostMapping("/validate-ticket")
    public ResponseEntity validateTicket(@RequestBody TicketControlDTO ticketControlDTO) {
        boolean ok = ticketService.validateTicket(ticketControlDTO);
        if (ok) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
