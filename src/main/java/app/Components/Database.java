package app.Components;

import app.DAO.IssueDAO;
import app.DAO.PartimeWorkerDAO;
import app.DAO.TicketDAO;
import app.Enums.Category;
import app.Models.Issue;
import app.Models.PartimeWorker;
import app.Models.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class Database {
    private PartimeWorkerDAO partimeWorkerDAO;
    private IssueDAO issueDAO;
    private TicketDAO ticketDAO;

    @Autowired
    public Database(PartimeWorkerDAO partimeWorkerDAO, IssueDAO issueDAO, TicketDAO ticketDAO) {
        this.partimeWorkerDAO = partimeWorkerDAO;
        this.issueDAO = issueDAO;
        this.ticketDAO = ticketDAO;
    }

    /**
     * Insert mock data
     */
    @Transactional
    public void insertDataToDatabase() {
        PartimeWorker worker1 = new PartimeWorker("Tom", "123123123", "svobot20@fit.cvut.cz");
        partimeWorkerDAO.save(worker1);
        PartimeWorker worker2 = new PartimeWorker("Pepa", "234234234", "pepek12@fit.cvut.cz");
        partimeWorkerDAO.save(worker2);

        issueDAO.save(new Issue(Category.SECURITY, true, "Rogue intruder.", worker1));

        ticketDAO.save(new Ticket("a"));
        ticketDAO.save(new Ticket("b"));
        ticketDAO.save(new Ticket("c"));

        System.out.println("== Database data LOADED ==");
    }
}
