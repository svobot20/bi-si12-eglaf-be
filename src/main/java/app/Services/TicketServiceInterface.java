package app.Services;

import app.DTO.TicketControlDTO;

public interface TicketServiceInterface {

    /**
     * Check if ticket is valid
     *
     * @param ticketControlDTO ticket that is being validated
     * @return true if ticket is valid
     */
    boolean validateTicket(TicketControlDTO ticketControlDTO);

}
