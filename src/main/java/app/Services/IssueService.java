package app.Services;

import app.DAO.IssueDAO;
import app.DAO.PartimeWorkerDAO;
import app.DTO.CancelIssueDTO;
import app.DTO.IssueDTO;
import app.DTO.IssueManipulationDTO;
import app.DTO.ReportIssueDTO;
import app.Enums.IssueState;
import app.Models.Issue;
import app.Models.PartimeWorker;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class IssueService implements IssueServiceInterface {

    private IssueDAO issueDAO;
    private PartimeWorkerDAO partimeWorkerDAO;
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public IssueService(IssueDAO issueDAO, PartimeWorkerDAO partimeWorkerDAO) {
        this.issueDAO = issueDAO;
        this.partimeWorkerDAO = partimeWorkerDAO;
    }

    public boolean cancelIssue(CancelIssueDTO cancelIssueDTO) {
        Issue issue = issueDAO.getOne(cancelIssueDTO.getIssueId());
        if (issue == null || issue.getState() != IssueState.OPEN || issue.getPartimeWorker() != null) {
            // issue is not open or is assigned
            return false;
        } else {
            issue.setState(IssueState.CANCELED);
            issueDAO.save(issue);
            return true;
        }
    }

    public boolean resolveIssue(IssueManipulationDTO issueManipulationDTO) {
        // TODO: use session to get user
        PartimeWorker user = partimeWorkerDAO.getOne(issueManipulationDTO.getUserId());
        Issue issue = issueDAO.getOne(issueManipulationDTO.getIssueId());
        if (issue == null || user == null || issue.getState() != IssueState.OPEN || !user.equals(issue.getPartimeWorker())) {
            // issue is not open or is assigned
            return false;
        } else {
            issue.setState(IssueState.RESOLVED);
            issueDAO.save(issue);
            return true;
        }
    }

    public void reportIssue(ReportIssueDTO reportIssueDTO) {
        PartimeWorker reporter = partimeWorkerDAO.getOne(reportIssueDTO.getUid());
        Issue issue = new Issue(reportIssueDTO.getCategory(), reportIssueDTO.getUrgent(), reportIssueDTO.getInfo(), reporter);
        issueDAO.save(issue);
    }

    public boolean unclaimIssue(IssueManipulationDTO issueManipulationDTO) {
        // TODO: use session to get user
        PartimeWorker user = partimeWorkerDAO.getOne(issueManipulationDTO.getUserId());
        Issue issue = issueDAO.getOne(issueManipulationDTO.getIssueId());

        if (user == null || issue.getState() != IssueState.OPEN || !user.equals(issue.getPartimeWorker())) {
            // Issue is not assigned to user
            return false;
        } else {
            issue.setPartimeWorker(null);
            issueDAO.save(issue);
            return true;
        }
    }

    public boolean claimIssue(IssueManipulationDTO issueManipulationDTO) {
        // TODO: use session to get user
        PartimeWorker user = partimeWorkerDAO.getOne(issueManipulationDTO.getUserId());
        Issue issue = issueDAO.getOne(issueManipulationDTO.getIssueId());

        if (user == null || issue.getState() == IssueState.RESOLVED || issue.getPartimeWorker() != null) {
            // Issue is taken or already resolved
            return false;
        } else {
            issue.setPartimeWorker(user);
            issueDAO.save(issue);
            return true;
        }
    }

    public List<IssueDTO> getClaimedIssues(Long userId) {
        // TODO: get user data from session
        PartimeWorker user = partimeWorkerDAO.getOne(userId);
        List<Issue> issues = user.getIssues();

        Type listType = new TypeToken<List<IssueDTO>>() {
        }.getType();
        return modelMapper.map(issues, listType);
    }

    public List<IssueDTO> getOpenIssues() {
        List<Issue> all = issueDAO.getActiveIssues();

        Type listType = new TypeToken<List<IssueDTO>>() {
        }.getType();
        return modelMapper.map(all, listType);
    }
}
