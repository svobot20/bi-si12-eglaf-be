package app.Services;

import app.DAO.PartimeWorkerDAO;
import app.DTO.LoginDTO;
import app.DTO.PartimeWorkerDTO;
import app.Models.PartimeWorker;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService implements LoginServiceInterface {

    private PartimeWorkerDAO partimeWorkerDAO;
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public LoginService(PartimeWorkerDAO partimeWorkerDAO) {
        this.partimeWorkerDAO = partimeWorkerDAO;
    }

    public PartimeWorkerDTO login(LoginDTO loginDTO) {
        PartimeWorker user = partimeWorkerDAO.findPartimeWorker(loginDTO.getEmail());
        return modelMapper.map(user, PartimeWorkerDTO.class);
    }

}
