package app.Services;

import app.DTO.LoginDTO;
import app.DTO.PartimeWorkerDTO;

public interface LoginServiceInterface {

    /**
     * Login user to the app
     *
     * @param loginDTO login data
     * @return user information
     */
    PartimeWorkerDTO login(LoginDTO loginDTO);

}
