package app.Services;

import app.DTO.CancelIssueDTO;
import app.DTO.IssueDTO;
import app.DTO.IssueManipulationDTO;
import app.DTO.ReportIssueDTO;

import java.util.List;

public interface IssueServiceInterface {

    /**
     * Cancel Issue
     *
     * @param cancelIssueDTO issue to be canceled
     * @return operation successful
     */
    boolean cancelIssue(CancelIssueDTO cancelIssueDTO);

    /**
     * Resolve issue
     *
     * @param issueManipulationDTO issue to be resolved
     * @return operation successful
     */
    boolean resolveIssue(IssueManipulationDTO issueManipulationDTO);

    /**
     * Create new issue
     *
     * @param reportIssueDTO issue data
     */
    void reportIssue(ReportIssueDTO reportIssueDTO);

    /**
     * Unassign issue from user
     *
     * @param issueManipulationDTO issue to be unassigned
     * @return operation successful
     */
    boolean unclaimIssue(IssueManipulationDTO issueManipulationDTO);

    /**
     * Assign issue to user
     *
     * @param issueManipulationDTO issue and user to be paired
     * @return operation successful
     */
    boolean claimIssue(IssueManipulationDTO issueManipulationDTO);

    /**
     * Get issues claimed by user
     *
     * @param userId user id
     * @return list of claimed issues for user
     */
    List<IssueDTO> getClaimedIssues(Long userId);

    /**
     * Get available issues
     *
     * @return list of issues
     */
    List<IssueDTO> getOpenIssues();
}
