package app.Services;

import app.DAO.TicketDAO;
import app.DTO.TicketControlDTO;
import app.Models.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketService implements TicketServiceInterface {
    private TicketDAO ticketDAO;

    @Autowired
    public TicketService(TicketDAO ticketDAO) {
        this.ticketDAO = ticketDAO;
    }

    public boolean validateTicket(TicketControlDTO ticketControlDTO) {
        Ticket ticket = ticketDAO.getTicket(ticketControlDTO.getToken());
        if (ticket != null && !ticket.getScanned()) {
            ticket.setScanTime(System.currentTimeMillis());
            ticket.setScanned(true);
            ticketDAO.save(ticket);
            return true;
        }
        return false;
    }

}
