package app.Enums;

public enum IssueState {
    OPEN,
    RESOLVED,
    CANCELED,
}
