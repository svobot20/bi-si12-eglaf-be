package app.Enums;

public enum Category {
    CATERING, SECURITY, INFO_POINT, OTHERS, REGISTRATION
}
