package app.Models;

import app.Enums.Category;
import app.Enums.IssueState;

import javax.persistence.*;

@Entity
public class Issue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private IssueState state;

    @Column(length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private Category category;

    @Column(nullable = false)
    private Boolean urgent;

    @Column(length = 255, nullable = false)
    private String info;

    @ManyToOne(fetch = FetchType.EAGER)
    private PartimeWorker creator;

    @ManyToOne(fetch = FetchType.LAZY)
    private PartimeWorker partimeWorker;

    public Issue() {
    }

    public Issue(Category category, Boolean urgent, String info, PartimeWorker creator) {
        this.category = category;
        this.urgent = urgent;
        this.info = info;
        this.creator = creator;
        this.state = IssueState.OPEN;
    }

    public PartimeWorker getCreator() {
        return creator;
    }

    public void setCreator(PartimeWorker creator) {
        this.creator = creator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IssueState getState() {
        return state;
    }

    public void setState(IssueState state) {
        this.state = state;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getUrgent() {
        return urgent;
    }

    public void setUrgent(Boolean urgent) {
        this.urgent = urgent;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public PartimeWorker getPartimeWorker() {
        return partimeWorker;
    }

    public void setPartimeWorker(PartimeWorker partimeWorker) {
        this.partimeWorker = partimeWorker;
    }

}
