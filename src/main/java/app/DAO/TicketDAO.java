package app.DAO;

import app.Models.Ticket;

public interface TicketDAO {
    /**
     * Get ticket by token
     *
     * @param token value
     * @return ticket
     */
    Ticket getTicket(String token);

    /**
     * Update / create ticket
     *
     * @param ticket ticket data
     */
    void save(Ticket ticket);
}
