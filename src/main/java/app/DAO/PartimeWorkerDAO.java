package app.DAO;

import app.Models.PartimeWorker;

public interface PartimeWorkerDAO {

    /**
     * Find worker by its email address
     *
     * @param email worker email
     * @return worker
     */
    PartimeWorker findPartimeWorker(String email);

    PartimeWorker getOne(Long id);

    void save(PartimeWorker partimeWorker);
}
