package app.DAO;

import app.Enums.IssueState;
import app.Models.Issue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IssueRepository extends JpaRepository<Issue, Long> {
    List<Issue> findAllByStateEqualsAndPartimeWorkerIsNull(IssueState issueState);
}
