package app.DAO;

import app.Enums.IssueState;
import app.Models.Issue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcIssueDAO implements IssueDAO {
    private IssueRepository issueRepository;

    @Autowired
    public JdbcIssueDAO(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    public Issue save(Issue issue) {
        return issueRepository.save(issue);
    }

    public List<Issue> getActiveIssues() {
        return issueRepository.findAllByStateEqualsAndPartimeWorkerIsNull(IssueState.OPEN);
    }

    public Issue getOne(Long id) {
        return issueRepository.getOne(id);
    }
}
