package app.DAO;

import app.Models.PartimeWorker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartimeWorkerRepository extends JpaRepository<PartimeWorker, Long> {

    PartimeWorker findAppUserByEmail(String email);

}
