package app.DAO;

import app.Models.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcTicketDAO implements TicketDAO {

    private TicketRepository ticketRepository;

    @Autowired
    public JdbcTicketDAO(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public Ticket getTicket(String token) {
        return ticketRepository.getByQrToken(token);
    }

    public void save(Ticket ticket) {
        ticketRepository.save(ticket);
    }
}
