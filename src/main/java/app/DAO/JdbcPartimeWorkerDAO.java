package app.DAO;

import app.Models.PartimeWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcPartimeWorkerDAO implements PartimeWorkerDAO {

    private PartimeWorkerRepository partimeWorkerRepository;

    @Autowired
    public JdbcPartimeWorkerDAO(PartimeWorkerRepository partimeWorkerRepository) {
        this.partimeWorkerRepository = partimeWorkerRepository;
    }

    public PartimeWorker findPartimeWorker(String email) {
        return partimeWorkerRepository.findAppUserByEmail(email);
    }

    public PartimeWorker getOne(Long id) {
        return partimeWorkerRepository.getOne(id);
    }

    public void save(PartimeWorker partimeWorker) {
        partimeWorkerRepository.save(partimeWorker);
    }
}
