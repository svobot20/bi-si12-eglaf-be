package app.DAO;


import app.Models.Issue;

import java.util.List;

public interface IssueDAO {
    /**
     * Save issue into database (create or update)
     *
     * @param issue to be saved
     * @return stored issue
     */
    Issue save(Issue issue);

    /**
     * Get issue by id
     * @param id of issue
     * @return issue
     */
    Issue getOne(Long id);

    /**
     * Get active issues
     * @return list of active issues
     */
    List<Issue> getActiveIssues();
}
