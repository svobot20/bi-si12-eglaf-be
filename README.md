### Eglaf

Backend application (packaged application with Apache server)

## Build package
To create runnable jar (app has packed Apache server):
1. set up db connection in application.properties e.g.:
```
spring.datasource.url=jdbc:postgresql://localhost/eglaf
spring.datasource.userName=eglaf_user
spring.datasource.password=eglaf_user
```

2. build
```$ mvn package```

## Run server
1. set up postgres db

2. run server
```$ java -jar PACKAGE```